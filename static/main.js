var localesBound = false;
var forms = [
  {
    selector: 'form.edit-comment',
  },
  {
    selector: 'form.edit-user',
  },
  {
    selector: 'form.edit-self',
  },
  {
    selector: 'form.edit-task',
  },
  {
    selector: 'form.form-login',
  }
];

var getFormFields = function(formSelector) {
  var form = $(formSelector);
  return form.find('input, select, textarea');
};

var validateFieldAgainstSpec = function(field, fieldSpec) {
  return true;
};

var validateFieldAgainstType = function(field, value) {
  if (field.type === 'email') {
    var match = value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/);
    if (!match) {
      return {
        field: field.name,
        message: 'Field "${fieldLabel}" must be a valid e-mail address',
      };
    }
  }
  return true;
};

var validateField = function(field, fieldSpec) {
  var jqField = $(field);
  var value = jqField.val();

  if (field.required && !value) {
    return {
      field: field.name,
      message: 'Field "${fieldLabel}" is required',
    };
  }

  var typeError = validateFieldAgainstType(field, value);

  if (typeError !== true) {
    return typeError;
  }

  return fieldSpec ? validateFieldAgainstSpec(field, fieldSpec) : true;
};

var getFieldSpec = function(formSpec, fieldName) {
  return formSpec.fields && formSpec.fields[fieldName] ?
    formSpec.fields[fieldName] : null;
};

var mapFieldToValidation = function(formSpec) {
  return function(field) {
    return validateField(field, getFieldSpec(formSpec, field.name))
  };
};

var filterInvalid = function(result) {
  return result !== true;
};

var getFieldLabel = function(form, fieldName) {
  var fieldLabel = form.find('[for=' + fieldName + ']');
  return fieldLabel ? fieldLabel.text().replace('*', '').trim() : null;
};

var renderFieldError = function(form, error) {
  var field = form.find('[name=' + error.field + ']');
  var errorElement = $('<div class="invalid-feedback" />');
  var message = error.message
    .replace(/\$\{fieldName\}/g, error.field)
    .replace(/\$\{fieldLabel\}/g, getFieldLabel(form, error.field));
  errorElement.text(message);
  field.addClass('is-invalid');
  field.parent().append(errorElement);
};

var renderFormErrors = function(formSpec, errors) {
  var form = $(formSpec.selector);
  form.find('.invalid-feedback').remove();

  for (var index = 0; index < errors.length; index++) {
    renderFieldError(form, errors[index]);
  }
};

var validateForm = function(formSpec) {
  return function(e) {
    var fields = getFormFields(formSpec.selector).toArray();
    var errors = fields.map(mapFieldToValidation(formSpec)).filter(filterInvalid);

    if (errors.length > 0) {
      e.preventDefault();
      console.info('Form submit prevented because of validation error');
      renderFormErrors(formSpec, errors);
    }
  };
};

var bindFormValidators = function() {
  for (var index = 0; index < forms.length; index++) {
    var formSpec = forms[index];
    var form = $(formSpec.selector);

    if (form.length !== 0) {
      form.submit(validateForm(formSpec));
      // Replace HTML 5 validation with defined validators
      form.attr('novalidate', true);
      console.info('Connected form', formSpec.selector);
    }
  }
};

var renderChart = function(el, response) {
  var created = {
    name: 'Created',
    data: [],
  };
  var updated = {
    name: 'Updated',
    data: [],
  };
  var archived = {
    name: 'Archived',
    data: [],
  };
  var commented = {
    name: 'Commented',
    data: [],
  };
  var series = [created, updated, archived, commented];
  for (var index = 0; index < response.data.length; index++) {
    var day = response.data[index];
    // var date = new Date(day.date);
    var dateSplit = day.date.split('-');
    var date = Date.UTC(dateSplit[0], dateSplit[1]-1, dateSplit[2]);
    created.data.push([date, day.created]);
    updated.data.push([date, day.updated]);
    archived.data.push([date, day.archived]);
    commented.data.push([date, day.commented]);
  }
  Highcharts.chart(el, {
    title: null,
    chart: {
      type: 'spline',
    },
    credits: {
      enabled: false,
    },
    xAxis: {
      type: 'datetime',
      title: {
        text: 'Date',
      },
      labels: {
        formatter: function() {
          return Highcharts.dateFormat("%b %e", this.value);
        },
      },
    },
    yAxis: {
      title: {
        text: 'Number of Tasks'
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },

    plotOptions: {
      spline: {
        marker: {
          enabled: true
        }
      }
    },
    series: series,
    responsive: {
      rules: [{
        condition: {
          maxWidth: 600
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
}

var bindCharts = function() {
  var el = $('.weekly-changes');

  if (el.length > 0) {
    var html = $('html');
    var prefix = html.data('url-prefix') || '';
    $.ajax({
      url: prefix + '/api/taskStats',
      success: function(el) {
        return function(data) {
          renderChart(el, data);
        };
      }(el[0])
    });
  }
};

var handleDelete = function(e) {
  if (!confirm('Are you sure you want to delete that?')) {
    e.preventDefault();
  }
}

var bindDeleteButtons = function() {
  $('.btn-delete').click(handleDelete);
};

var bindDateDisplay = function() {
  $('time.date').each(function(index, element) {
    var $element = $(element);
    $element.text(moment($element.attr('datetime')).format('LLLL'));
  });
};

var bindDateTimeDisplay = function() {
  $('time.datetime').each(function(index, element) {
    var $element = $(element);
    var date = moment($element.attr('datetime'));
    var dateText = date.fromNow();

    if (date.isBefore(moment().subtract(2, 'week'))) {
      dateText = date.format('LL')
    }
    $element.text(dateText).attr('title', date.format('L LTS'));
  });
};

hljs.initHighlightingOnLoad();

var getBrowserLanguage = function() {
  if (navigator.languages) {
    return navigator.languages[0];
  }
  return navigator.language;
};

var redrawLocales = function() {
  moment.locale(getBrowserLanguage());
  bindDateDisplay();
  bindDateTimeDisplay();
};

var bindLocales = function() {
  redrawLocales();
  if (!localesBound) {
    if (typeof window.onlanguagechange !== 'undefined') {
      window.addEventListener('languagechange', redrawLocales);
    }
  }
};

var bindMainMenu = function() {
  $('#appNavbar').addClass('collapse');
};

$(function() {
  bindMainMenu();
  bindDeleteButtons();
  bindFormValidators();
  bindCharts();
  bindLocales();
  console.log('loaded');
});
