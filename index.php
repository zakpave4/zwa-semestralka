<?php

require_once 'vendor/autoload.php';
session_start();
$config = Defiant\Runner::getConfig();
$runner = new Defiant\Runner($config);
$request = Defiant\Http\Request::fromGlobals();
$runner->resolveRequest($request);
