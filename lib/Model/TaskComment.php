<?php

/**
 * TaskComment model
 */
namespace Totodo\Model;

/**
 * TaskComment model used for storing user comments to the task
 */
class TaskComment extends \Defiant\Model {
  /** @var \Defiant\Model\Field[] $fields Field definition */
  protected static $fields = [
    'task' => [
      "type" => '\Defiant\Model\ForeignKeyField',
      "model" => "\Totodo\Model\Task",
      "relatedName" => "comments",
    ],
    'author' => [
      "type" => '\Defiant\Model\ForeignKeyField',
      "model" => "\Totodo\Model\User",
      "relatedName" => "comments",
    ],
    'text' => '\Defiant\Model\TextField',
  ];
}
