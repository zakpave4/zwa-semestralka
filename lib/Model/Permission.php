<?php

/**
 * Permission model
 */
namespace Totodo\Model;

/**
 * Permission model used for defining permissions
 */
class Permission extends \Defiant\Model {
  /** @var \Defiant\Model\Field[] $fields Field definition */
  protected static $fields = [
    'name' => '\Defiant\Model\VarcharField',
    'role' => [
      'type' => '\Defiant\Model\ForeignKeyField',
      'model' => '\Totodo\Model\Role',
    ],
  ];
}
