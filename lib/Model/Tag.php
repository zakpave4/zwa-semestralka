<?php

/**
 * Tag model
 */
namespace Totodo\Model;

/**
 * Tag model used for tagging tasks
 */
class Tag extends \Defiant\Model {
  /** @var \Defiant\Model\Field[] $fields Field definition */
  protected static $fields = [
    'name' => '\Defiant\Model\VarcharField',
    'tasks' => [
      'fk' => 'tag',
      'type' => '\Defiant\Model\ManyToManyField',
      'model' => 'Totodo\Model\Task',
      'trough' => 'Totodo\Model\TaskTag',
      'via' => 'task',
    ],
  ];
}
