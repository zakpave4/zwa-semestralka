<?php

/**
 * TaskStatus model
 */
namespace Totodo\Model;

/**
 * TaskStatus model used for storing possible task states
 */
class TaskStatus extends \Defiant\Model {
  /** @var \Defiant\Model\Field[] $fields Field definition */
  protected static $fields = [
    'name' => '\Defiant\Model\VarcharField',
    'weight' => '\Defiant\Model\PositiveIntegerField',
  ];
}
