<?php

/**
 * Simplifies exporting models
 */

/**
 * Require all the models
 */
require_once 'Permission.php';
require_once 'Role.php';
require_once 'Tag.php';
require_once 'Task.php';
require_once 'TaskComment.php';
require_once 'TaskChange.php';
require_once 'TaskStatus.php';
require_once 'TaskTag.php';
require_once 'User.php';
require_once 'UserRole.php';
