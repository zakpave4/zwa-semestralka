<?php

/**
 * Manage user session - login/logout
 */
namespace Totodo\View;

/**
 * View class for authenticating users
 */
class Login extends \Defiant\View {
  /**
   * View method used to render login for and for authenticating. Redirects to
   * home page with message about login success.
   *
   * @return string|void Returns rendered template or nothing when login is
   *   successful and user gets redirected.
   */
  public function index() {
    $errors = [];
    $values = [];
    $user = $this->getUser();

    if ($user) {
      return $this->redirect($this->url('home'));
    }

    if ($this->request->method == 'post') {
      $form = \Defiant\View\GenericForm::fromModel('\Totodo\Model\User', [
        'email',
        'password',
      ]);

      try {
        $values = $form->getValidatedData($this->request);
      } catch(\Defiant\View\ValidationErrorCollection $e) {
        $errors = $e->toErrorList();
        $values = $e->getRawData();
      }

      if (sizeof($errors) === 0) {
        try {
          $user = $this->login($values['email'], $values['password']);
        } catch(\Defiant\Model\FieldError $e) {
          $errors['password'] = $e->getMessage();
        } catch(\Defiant\Model\Error $e) {
          $errors['email'] = $e->getMessage();
        }
      }

      if (sizeof($errors) === 0) {
        $homeUrl = $this->url('home', ['message' => 'login']);
        $redirectTo = $this->request->getBodyParam('redirectTo', $homeUrl);
        if ($redirectTo === $this->request->getPath()) {
          $redirectTo = $homeUrl;
        }
        $user->lastLogin = new \DateTime();
        $user->save();
        $this->redirect($redirectTo);
      }
    }

    return $this->renderTemplate('login.pug', [
      "errors" => $errors,
      "values" => $values,
    ]);
  }

  /**
   * View method used for logging user out. Redirects user to home page with
   * message about logout success
   *
   * @return void
   */
  public function logout() {
    $this->logoutUser();
    $this->redirect($this->url('home', [
      'message' => 'logout',
    ]));
  }
}
