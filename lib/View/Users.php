<?php

/**
 * Manage users
 */

namespace Totodo\View;

/**
 * View class for managing Users
 */
class Users extends \Defiant\View {
  const FORM_FIELDS = [
    'name',
    'email',
    'roles',
  ];

  /**
   * View API method determining accessibility of this view. Grants access
   * only to users with permission 'manage-users'.
   *
   * @return bool
   */
  public function isAccessible() {
    return !!$this->getUser();
  }
  /**
   * Page size for user list
   */
  const PAGE_SIZE = 10;

   /**
    * View helper method that reads userId from request params and finds the
    * corresponding User.
    *
    * @throws Defiant\Http\NotFound when the ID is empty or user does not exist
    * @return Totodo\Model\User
    */
   protected function fetchUser() {
     $userId = $this->getParam('userId');
     $user = null;
     if ($userId) {
       $user = $this->models->user->objects->find($userId);
     }
     if (!$userId || !$user) {
       throw new \Defiant\Http\NotFound();
     }
     return $user;
   }

  /**
   * Displays user list with paging
   *
   * @return string
   */
  public function index() {
    $page = $this->request->query('page', 1);
    $urlParams = $this->request->getQuery();
    $userQuery = $this->models->user->objects
      ->distinct()
      ->orderBy('-updatedAt, -id');

    $total = $userQuery->clone()->count();
    $users = $userQuery->limitByPage($page - 1, static::PAGE_SIZE)->all();
    $totalPages = ceil($total/static::PAGE_SIZE);
    return $this->renderTemplate('users/list.pug', [
      'message' => $this->request->query('message', null),
      'users' => $users,
      'urlParams' => $urlParams,
      'paginatePage' => $page,
      'paginatePrevPage' => max(1, $page - 1),
      'paginateNextPage' => min($totalPages, $page + 1),
      'paginateBaseUrl' => 'user:list',
      'paginateUrlParms' => $urlParams,
      'paginateShownItems' => sizeof($users),
      'paginateTotalPages' => ceil($total/static::PAGE_SIZE),
      'paginateTotalItems' => $total,
    ]);
  }

  /**
   * Displays user detail
   *
   * @return string
   */
  public function detail() {
    $userTasksMax = 7;
    $userDetail = $this->fetchUser();
    $recentHistory = $userDetail->changes->limit(0, 15)->all();
    $tasksQuery = $userDetail->tasks->filter(['archived' => false]);
    $userTasks = $tasksQuery->clone()->limit(0, $userTasksMax)->all();
    $userTasksTotal = $tasksQuery->count();
    return $this->renderTemplate('users/detail.pug', [
      'message' => $this->request->query('message', null),
      'recentHistory' => $recentHistory,
      'userDetail' => $userDetail,
      'userTasks' => $userTasks,
      'userTasksMax' => $userTasksMax,
      'userTasksTotal' => $userTasksTotal,
    ]);
  }


  /**
   * Displays list of tasks assigned to the user with paging
   *
   * @return string
   */
  public function tasks() {
    return \Totodo\View\Tasks::taskList(
      $this,
      $this->request,
      $this->models,
      $this->fetchUser()->tasks
    );
  }
}
