<?php

/**
 * Create and change tasks
 */


namespace Totodo\View;

/**
 * View class for creating and changing Tasks
 */
class TaskOperations extends \Defiant\View {
  const FORM_FIELDS = [
    'name',
    'description',
    'due',
    'tags',
  ];

  /**
   * View API method determining accessibility of this view. Refuses access
   * to unidentified users.
   *
   * @return bool
   */
  public function isAccessible() {
    return !!$this->getUser();
  }

  /**
   * View method used to render edit task form, create and edit tasks. Saves
   * changes to database and redirects back to user detail with result message.
   *
   * @throws Defiant\Http\NotFound when taskId is passed, but task does not exist
   * @return string|void Returns rendered create/edit page or nothing on
   *   redirect
   */
  public function edit() {
    $mode = 'create';
    $values = ['tags' => []];
    $task = null;
    $taskId = $this->request->getParam('taskId', null);
    if ($taskId) {
      $mode = 'edit';
      $task = $this->models->task->objects->find($taskId);
      if (!$task) {
        throw new \Defiant\Http\NotFound();
      }
      if ($task->archived) {
        throw new \Defiant\Http\Forbidden('Task has been archived');
      }
      $values = $task->serializeFields(static::FORM_FIELDS);
    }
    $errors = [];
    $method = $this->request->getMethod();

    if ($method === 'post') {
      $form = \Defiant\View\GenericForm::fromModel($this->models->task->model, static::FORM_FIELDS);
      try {
        $values = $form->getValidatedData($this->request);
      } catch(\Defiant\View\ValidationErrorCollection $e) {
        $errors = $e->toErrorList();
        $values = $e->getRawData();
      }

      if (sizeof($errors) === 0) {
        if ($mode == 'edit') {
          $message = 'changed';
          $prevState = $task->serializeFields(static::FORM_FIELDS);
          $task->setState($values);
          $task->save();
          if ($task->hasChanged()) {
            $currentState = $task->serializeFields(static::FORM_FIELDS);
            $task->log(
              $this->getUser(),
              \Totodo\Model\TaskChange::ACTION_CHANGE,
              $prevState,
              $currentState
            );
          }
        } else {
          $message = 'created';
          $values['authorId'] = $this->getUser()->id;
          $task = $this->models->task->create($values);
          $task->save();
        }
        return $this->redirect($this->url('task:detail', [
          'taskId' => $task->id,
          'message' => $message,
        ]));
      }
    } else {
      $queryTags = $this->request->query('tags', null);
      if ($queryTags) {
        $values['tags'] = $queryTags;
      }
    }

    $tagsAll = $this->models->tag->objects->orderBy('name')->all();
    $tagOptions = [];

    foreach ($tagsAll as $tag) {
      $tagOptions[] = [
        'label' => $tag->name,
        'value' => $tag->id,
      ];
    }

    return $this->renderTemplate('tasks/edit.pug', [
      'errors' => $errors,
      'tagOptions' => $tagOptions,
      'task' => $task,
      'values' => $values,
    ]);
  }

  /**
   * View method used to archive task. Redirects back to task detail with
   * result message on success.
   *
   * @throws Defiant\Http\NotFound when task is not found or missing taskId
   * @throws Defiant\Http\Forbidden when task is already archived
   * @return void
   */
  public function archive() {
    return $this->archiveToggle(true);
  }

  /**
   * View method used to unarchive task. Redirects back to task detail with
   * result message on success.
   *
   * @throws Defiant\Http\NotFound when task is not found or missing taskId
   * @throws Defiant\Http\Forbidden when task is already unarchived
   * @return void
   */
  public function unarchive() {
    return $this->archiveToggle(false);
  }

  /**
   * View helper method used to toggle task archived status. Redirects back to
   * task detail with result message on success.
   *
   * @param $nextStatus boolean True to archive, false to unarchive
   * @throws Defiant\Http\NotFound when task is not found or missing taskId
   * @throws Defiant\Http\Forbidden when task is already un/archived
   * @return void
   */
  protected function archiveToggle($nextStatus) {
    $task = $this->getTask();

    if ($task->archived == $nextStatus) {
      throw new \Defiant\Http\Forbidden(
        $nextStatus ?
          'Cannot archive task when it is already archived' :
          'Cannot unarchive task when it is already unarchived'
      );
    }

    $change = \Totodo\Model\TaskChange::ACTION_ARCHIVE;
    $message = 'archived';

    if (!$nextStatus) {
      $change = \Totodo\Model\TaskChange::ACTION_UNARCHIVE;
      $message = 'unarchived';
    }

    $prevState = $task->serializeFields(static::FORM_FIELDS);
    $task->archived = $nextStatus;
    $task->save();
    $currentState = $task->serializeFields(static::FORM_FIELDS);

    if ($task->hasChanged()) {
      $task->log(
        $this->getUser(),
        $change,
        $prevState,
        $currentState
      );
    }

    return $this->redirect($this->url('task:detail', [
      'taskId' => $task->id,
      'message' => $message,
    ]));
  }

  /**
   * View method used to render comment edit form page. Saves comment to
   * database when it is posted and redirects to task detail with result message
   * and highlight id.
   *
   * @throws Defiant\Http\Forbidden when task is archived
   * @throws Defiant\Http\NotFound when task is not found or missing taskId
   * @return string|void
   */
  public function comment() {
    $task = $this->getTask();
    $replyToId = $this->request->query('replyTo', null);
    $replyTo = null;

    if ($task->archived) {
      throw new \Defiant\Http\Forbidden('Cannot comment archived task');
    }

    $values = [];
    $errors = [];

    if ($replyToId) {
      $replyTo = $this->models->taskComment->objects->find($replyToId);

      if (!$replyTo) {
        throw new \Defiant\Http\NotFound('Cannot reply to non-existent comment');
      }

      $values['text'] = $this->formatReply($replyTo->text);
    }

    if ($this->request->getMethod() === 'post') {
      $form = \Defiant\View\GenericForm::fromModel('\Totodo\Model\TaskComment', [
        'text',
      ]);
      try {
        $values = $form->getValidatedData($this->request);
      } catch(\Defiant\View\ValidationErrorCollection $e) {
        $errors = $e->toErrorList();
        $values = $e->getRawData();
      }

      if (sizeof($errors) === 0) {
        $values['taskId'] = $task->id;
        $values['authorId'] = $this->getUser()->id;
        $comment = $this->models->taskComment->create($values);
        $comment->save();
        $task->log(
          $this->getUser(),
          \Totodo\Model\TaskChange::ACTION_COMMENT,
          null,
          [
            'commentId' => $comment->id,
            'name' => $task->name,
            'text' => $comment->text,
          ]
        );
        return $this->redirect($this->url('task:detail', [
          'taskId' => $task->id,
          'highlight' => $comment->id,
          'message' => 'commented',
        ]));
      }
    }

    return $this->renderTemplate('tasks/comment-edit.pug', [
      'errors' => $errors,
      'replyTo' => $replyTo,
      'task' => $task,
      'values' => $values,
    ]);
  }

  /**
   * View method used to render comment edit form page. Saves comment to
   * database when it is posted and redirects to task detail with result message
   * and highlight id.
   *
   * @throws Defiant\Http\Forbidden when task is archived
   * @throws Defiant\Http\NotFound when task is not found or missing taskId
   * @return string|void
   */
  public function assign() {
    $task = $this->getTask();
    $replyToId = $this->request->query('replyTo', null);
    $replyTo = null;

    if ($task->archived) {
      throw new \Defiant\Http\Forbidden('Cannot assign archived task');
    }

    $values = ['assigned' => $task->assignedId];
    $errors = [];

    if ($this->request->getMethod() === 'post') {
      $form = \Defiant\View\GenericForm::fromModel('\Totodo\Model\Task', [
        'assigned',
      ]);
      try {
        $values = $form->getValidatedData($this->request);
      } catch(\Defiant\View\ValidationErrorCollection $e) {
        $errors = $e->toErrorList();
        $values = $e->getRawData();
      }

      if (sizeof($errors) === 0) {
        $currentState = ['name' => $task->name];
        $prevState = [];
        $prev = $task->assigned;

        if ((!$prev && !$values['assigned']) || ($prev && $prev->id == $values['assigned'])) {
          return $this->redirect($this->url('task:detail', [
            'taskId' => $task->id,
            'message' => 'no-change',
          ]));
        }

        if ($prev) {
          $prevState['assignedId'] = $prev->id;
          $prevState['assignedName'] = $prev->name;
        }

        $task->assignedId = $values['assigned'];
        $task->save();
        $current = $task->assigned;

        if ($current) {
          $currentState['assignedId'] = $current->id;
          $currentState['assignedName'] = $current->name;
        }

        $changeType = \Totodo\Model\TaskChange::ACTION_CHANGE_ASSIGN;
        $task->log($this->getUser(), $changeType, $prevState, $currentState);
        return $this->redirect($this->url('task:detail', [
          'taskId' => $task->id,
          'message' => 'assigned',
        ]));
      }
    }

    $users = $this->models->user->objects->all();
    $userOptions = [];

    foreach ($users as $user) {
      $userOptions[] = [
        'label' => $user->name,
        'value' => $user->id,
      ];
    }

    return $this->renderTemplate('tasks/assign.pug', [
      'errors' => $errors,
      'task' => $task,
      'userOptions' => $userOptions,
      'values' => $values,
    ]);
  }

  /**
   * View helper method that reads taskId from request params and finds the
   * corresponding Task.
   *
   * @throws Defiant\Http\NotFound when the ID is empty or task does not exist
   * @return Totodo\Model\Task
   */
  protected function getTask() {
    $taskId = $this->getParam('taskId');
    $task = null;
    if ($taskId) {
      $task = $this->models->task->objects->find($taskId);
    }
    if (!$taskId || !$task) {
      throw new \Defiant\Http\NotFound();
    }
    return $task;
  }

  /**
   * Format comment text into response string to be quoted.
   *
   * @param $text string Text to be formatted as quotation of reply
   * @return string
   */
  protected function formatReply($text) {
    $lines = explode("\n", $text);
    $allowedLines = [];
    foreach ($lines as $line) {
      if (strpos($line, '>') === 0) {
        continue;
      }
      $str = trim($line);
      if ($str != '') {
        $allowedLines[] = $str;
      }
    }

    return '> '.implode("\n> ", $allowedLines)."\n\n";
  }

  /**
   * Changes task status to specified one and redirects
   *
   * @throws Defiant\Http\NotFound when task or status does not exist
   * @return void
   */
  public function changeStatus() {
    $taskId = $this->request->getParam('taskId', null);
    $statusId = $this->request->getParam('statusId', null);

    if (!$taskId || !$statusId) {
      throw new \Defiant\Http\NotFound();
    }

    $task = $this->models->task->objects->find($taskId);
    $nextStatus = $this->models->taskStatus->objects->find($statusId);

    if (!$task || !$nextStatus) {
      throw new \Defiant\Http\NotFound();
    }

    $prevStatus = $task->status;
    if ($prevStatus->id != $nextStatus->id) {
      $task->status = $nextStatus;
      $task->save();
      $task->log(
        $this->getUser(),
        \Totodo\Model\TaskChange::ACTION_CHANGE_STATUS,
        ['status' => $prevStatus->name],
        ['name' => $task->name, 'status' => $nextStatus->name]
      );
      return $this->redirect($this->url('task:detail', [
        'taskId' => $task->id,
        'message' => 'changed',
      ]));
    }
    return $this->redirect($this->url('task:detail', [
      'taskId' => $task->id,
    ]));
  }
}
