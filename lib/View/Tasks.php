<?php

/**
 * View and list tasks, task comments and task history
 */


namespace Totodo\View;

/**
 * View class for public operations with tasks
 */
class Tasks extends \Defiant\View {
  /**
   * Page size for task list
   */
  const PAGE_SIZE = 10;
  /**
   * Page size for comments list
   */
  const PAGE_SIZE_DETAIL_COMMENTS = 6;
  /**
   * Page size for history list
   */
  const PAGE_SIZE_DETAIL_HISTORY = 15;

  /**
   * Displays public task list with paging
   *
   * @return string
   */
  public function index() {
    return static::taskList(
      $this,
      $this->request,
      $this->models,
      $this->models->task->objects
    );
  }

  /**
   * Displays public task list with paging
   *
   * @param \Defiant\View $view View that will be used to render template
   * @param \Defiant\Http\Request $request Request used to get params
   * @param \Defiant\Http\Collection $models Collection with connected models
   * @param \Defiant\Http\Query $taskQuery Filtered query for getting tasks
   * @return string
   */
  public static function taskList(
    \Defiant\View $view,
    \Defiant\Http\Request $request,
    \Defiant\Model\Collection $models,
    \Defiant\Model\Query $taskQuery
  ) {
    $page = $request->query('page', 1);
    $filterTagIds = $request->query('tags', []);
    $filterStatusId = $request->query('status', null);
    $filterTags = [];
    $urlParams = array_merge($request->getParams(), $request->getQuery());
    $taskQuery
      ->distinct()
      ->filter(['tags__contains' => $filterTagIds])
      ->orderBy('-updatedAt, -archived, -id');

    $tags = $models->tag
      ->objects->orderBy('name')->all();
    $statusList = $models->taskStatus
      ->objects->orderBy('weight')->all();
    $filterStatus = $models->taskStatus
      ->objects->find($filterStatusId);

    if ($filterStatus) {
      $taskQuery->filter(['status' => $filterStatus->id]);
    }

    $total = $taskQuery->clone()->count();
    $tasks = $taskQuery->limitByPage($page - 1, static::PAGE_SIZE)->all();
    foreach ($tags as $tag) {
      if (in_array((string) $tag->id, $filterTagIds)) {
        $filterTags[] = $tag;
      }
    }
    $totalPages = ceil($total/static::PAGE_SIZE);
    return $view->renderTemplate('tasks/list.pug', [
      'tasks' => $tasks,
      'tags' => $tags,
      'statusList' => $statusList,
      'filterTags' => $filterTags,
      'filterStatus' => $filterStatus,
      'urlParams' => $urlParams,
      'paginatePage' => $page,
      'paginatePrevPage' => max(1, $page - 1),
      'paginateNextPage' => min($totalPages, $page + 1),
      'paginateBaseUrl' => 'task:list',
      'paginateUrlParms' => $urlParams,
      'paginateShownItems' => sizeof($tasks),
      'paginateTotalPages' => ceil($total/static::PAGE_SIZE),
      'paginateTotalItems' => $total,
    ]);
  }

  /**
   * Displays public task detail with comments and history
   *
   * @throws Defiant\Http\NotFound When passed no :taskId or task does not exist
   * @return string
   */
  public function detail() {
    $task = $this->getTask();
    $statusList = $this->models->taskStatus->objects->all();
    $tags = $task->tags->all();
    $history = $task->changes->orderBy('-createdAt, -id')->limitByPage(0, static::PAGE_SIZE_DETAIL_HISTORY)->all();
    $historyLength = $task->changes->count();
    $comments = $task->comments->orderBy('-createdAt, -id')->limitByPage(0, static::PAGE_SIZE_DETAIL_COMMENTS)->all();
    $commentsLength = $task->comments->count();
    return $this->renderTemplate('tasks/detail.pug', [
      'comments' => $comments,
      'commentsLength' => $commentsLength,
      'commentHighlight' => $this->request->query('highlight', null),
      'history' => $history,
      'historyLength' => $historyLength,
      'message' => $this->request->query('message', null),
      'statusList' => $statusList,
      'tags' => $tags,
      'task' => $task,
    ]);
  }

  /**
   * Displays public list of task comments
   *
   * @throws Defiant\Http\NotFound When passed no :taskId or task does not exist
   * @return string
   */
  public function taskComments() {
    $task = $this->getTask();
    $commentsQuery = $task->comments;

    $comments = $commentsQuery->orderBy('-createdAt, -id')->all();
    return $this->renderTemplate('tasks/comments.pug', [
      'task' => $task,
      'comments' => $comments,
    ]);
  }

  /**
   * Returns task based on request params
   *
   * @throws Defiant\Http\NotFound When passed no :taskId or task does not exist
   * @return string
   */
  protected function getTask() {
    $taskId = $this->getParam('taskId');
    $task = $this->models->task->objects->find($taskId);
    if (!$taskId || !$task) {
      throw new \Defiant\Http\NotFound();
    }
    return $task;
  }
}
